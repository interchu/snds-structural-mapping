
# Mapping IR_NAT_V using CNAM statistique mensuelle and DREES
# database on individual expenditures ------------------

# PRS_NAT_REF is used in the VISIT_OCCURRENCE TABLE, as visit_source_value
# the mapping of IR_NAT_V here is therefore focused on the possible values
# of the visit domain
# for me there are a lot of other ways we could map IR_NAT_V
# to the different concepts
# PRS_NAT may also give info on condition_occurence


import pandas as pd
import numpy as np
import regex as re
import os
print(os.getcwd())
path2file_nom_base_rac = "C:/Users/raphaele.adjerad/Documents/analyse_nom/projet_gitlab/analyse_nomenclature_snds_oc/data"
path2file_concept = "C:/Users/raphaele.adjerad/Documents/projet_omop/" # CONCEPT.csv from Athena

# Import concept table ---------------
dict_type = {
    "concept_code": "str",
    "invalid_reason": "str"
}
concept = pd.read_csv(path2file_concept + "/CONCEPT.csv", sep="\t",
                      dtype=dict_type)
concept.columns
concept.shape  # 1093985

# Import nomenclature base rac ------
nom_base_rac = pd.read_csv(path2file_nom_base_rac + "/20191211_prs_nat_ref_presta_r_norme_b2.csv")
ir_nat_v = pd.read_csv(path2file_nom_base_rac + "/IR_NAT_V.csv")
nom_base_rac.head()
# we also keep norm B2 code so as to link with NGAP
ir_nat_v = ir_nat_v.loc[:,["PRS_NAT","PRS_NAT_CB2", "PRS_NAT_LIB"]]
nom_base_rac = nom_base_rac.loc[:,["PRS_NAT_REF","presta_R"]]
# join
ir_nat_v = (pd.merge(ir_nat_v, nom_base_rac, left_on="PRS_NAT",
                     right_on="PRS_NAT_REF", how = "left") \
            .drop(columns=["PRS_NAT_REF"]))
ir_nat_v.head(2)

# Now mapping under OMOP form ----------------
prs_nat_ref_mapping = pd.read_csv("resources\prs_nat_ref_mapping.csv")

name_dict = {
    "PRS_NAT": "local_concept_code",
    "PRS_NAT_LIB": "local_concept_name"
}
ir_nat_v = ir_nat_v.rename(columns=name_dict)
# Get the proposed_concept_id, name concept id and commentary from prs_nat_ref_mapping
prs_nat_matched = prs_nat_ref_mapping[prs_nat_ref_mapping["proposed_concept_id"].isnull()==False]
prs_nat_matched = prs_nat_matched.drop(columns=["local_concept_name"])
prs_nat_matched.local_concept_code = prs_nat_matched.local_concept_code.astype("int64")
ir_nat_v = pd.merge(ir_nat_v, prs_nat_matched,
                    how="left")

ir_nat_v.presta_R.unique().shape # 30 categories


# Functions to define proposed concept --------------------


def define_concept(condition, value_df):
    """
    Function that sets proposed_concept_id, concept_name and commentary
    based on boolean
    """
    ir_nat_v["proposed_concept_id"] = np.where(
        condition,
        value_df["concept_id"],
        ir_nat_v["proposed_concept_id"]
    )
    ir_nat_v["name_concept_id"] = np.where(
        condition,
        value_df["concept_name"],
        ir_nat_v["name_concept_id"]
    )
    ir_nat_v["commentary"] = np.where(
        condition,
        value_df["vocabulary_id"],
        ir_nat_v["commentary"]
    )
    return ir_nat_v


def visualize_table(condition):
    """
    Function to visualise PRS_NAT_REF associated with condition
    """
    return ir_nat_v.loc[condition,
                 ["local_concept_code", "PRS_NAT_CB2", "local_concept_name"]]

# VISIT DOMAIN ----------------------------------------

# The strategy is to break the problem into the categories
# defined in our expenditure database and explore the vocabularies
# Standard Visit Concepts are defined hierarchically, rolling up into 12 top concepts:
# This is the site we can find on OMOP website but it does not perfectly match the list in
# Athena vocab
# 581478 "Ambulance Visit"
# 38004193 "Case Management Visit"
# 262 "Emergency Room and Inpatient Visit"
# 9203 "Emergency Room Visit"
# 581476 "Home Visit"
# 38004311 "Inpatient Hospice"
# 9201 "Inpatient Visit"
# 32036 "Laboratory Visit"
# 42898160 "Non-hospital institution Visit"
# 9202 "Outpatient Visit"

# We can check those with : (Athena downloaded vocab)
concept.loc[concept["vocabulary_id"]=="Visit",
            ["concept_id", "concept_name", "vocabulary_id", "domain_id"]]
# when we filter on domain_id we actually get some other types of vocab
# but there are not listed as part of possible values for visit_concept_id on
# github doc so we will do with this

# Pharmacy visit ----------------

pharmacy = concept.loc[(concept["vocabulary_id"]=="Visit") &
            (concept.concept_name.str.contains("[Pp]harmacy")),
            ["concept_id", "concept_name", "vocabulary_id"]]
# to check see what are concepts with pharmacy :
test = concept.loc[(concept["concept_name"].isnull()==False)&
            (concept["concept_name"].str.contains("Pharmacy")),
            ["concept_id", "concept_name", "vocabulary_id"]]
ir_nat_v = define_concept(ir_nat_v["presta_R"].isin(["pharma", "pharma_15", "pharma_65",
                                                     "pha_hono_dispensation"]),
                          pharmacy)

# Labs ------------------------------------------

labs = concept.loc[(concept["vocabulary_id"]=="Visit") &
            (concept.concept_name.str.contains("[Ll]aboratory")),
            ["concept_id", "concept_name", "vocabulary_id", "domain_id"]]
# to check see what are concepts with labs :
test = concept.loc[(concept["concept_name"].isnull()==False)&
            (concept["concept_name"].str.contains("[Ll]aboratory")),
            ["concept_id", "concept_name", "vocabulary_id"]]

ir_nat_v = define_concept(ir_nat_v["presta_R"].isin(["Labo"]), labs)


# for cliniq and techniq, the fact that it is inpatient visit or outpatient visit
# or office visit depends on the location
# bear in mind that part of office visit
# take part in private sector hospitals and are counted in the private sector hospitals
# in French Health accounts
# to distinguish outpatient from inpatient, you would have to consider dates of begin and end
# for now let us just put inpatient id -> see following code for more specifics

inpatient = concept.loc[(concept["vocabulary_id"]=="Visit") &
            (concept.concept_name.str.startswith("Inpatient")),
            ["concept_id", "concept_name", "vocabulary_id"]]

ofvi = concept.loc[(concept["vocabulary_id"]=="Visit") &
            (concept.concept_name.str.contains("Office")),
            ["concept_id", "concept_name", "vocabulary_id"]]
hovi = concept.loc[(concept["vocabulary_id"]=="Visit") &
            (concept.concept_name.str.contains("Home Visit")),
            ["concept_id", "concept_name", "vocabulary_id"]]

cliniq_techn = pd.DataFrame({"concept_id": ["WHEN(ER_ETE_F__ETB_EXE_FIN.isNotNull(),{}).otherwise({})"
                        .format(inpatient.iloc[0,0], ofvi.iloc[0,0])],
                                "concept_name": ["WHEN(ER_ETE_F__ETB_EXE_FIN.isNotNull(),{}).otherwise({})"
                        .format(inpatient.iloc[0,1], ofvi.iloc[0,1])],
                                "vocabulary_id": ["WHEN(ER_ETE_F__ETB_EXE_FIN.isNotNull(),{}).otherwise({})"
                        .format(inpatient.iloc[0,2], ofvi.iloc[0,2])]})

test = visualize_table((ir_nat_v["presta_R"].isin(["techniq",
                                                   "cliniq"])))
ir_nat_v = define_concept(ir_nat_v["presta_R"].isin(["techniq",
                                                   "cliniq"]),
                          cliniq_techn)


# office visit  ----------------
# -> office visit begins with letter C in cliniq or techniq categories
test = ir_nat_v.loc[(ir_nat_v["presta_R"].isin(["cliniq","techniq"])) &
                    (ir_nat_v["PRS_NAT_CB2"].str.startswith("C")) &
                    (ir_nat_v["local_concept_name"].str.contains("Consultation",
                                                                 flags=re.IGNORECASE)),
                    ["PRS_NAT_CB2","local_concept_name"]]
ir_nat_v = define_concept((ir_nat_v["presta_R"].isin(["cliniq","techniq"])) &
                           (ir_nat_v["PRS_NAT_CB2"].str.startswith("C"))&
                           (ir_nat_v["local_concept_name"].str.contains("Consultation",
                                                                        flags=re.IGNORECASE)),
                           ofvi)

# home visit ---------------------------

# -> home visit begins with letter has "visite" in name in cliniq or techniq categories
test = ir_nat_v.loc[(ir_nat_v["presta_R"].isin(["cliniq","techniq"])) &
                    (ir_nat_v["PRS_NAT_CB2"].str.startswith("V")),
                    ["PRS_NAT_CB2","local_concept_name"]]
ir_nat_v = define_concept((ir_nat_v["presta_R"].isin(["cliniq","techniq"])) &
                          (ir_nat_v["local_concept_name"].str.contains("Visite",
                                                                       flags=re.IGNORECASE)),
                          hovi)

# Inpatient visit --------------------------
test = visualize_table((ir_nat_v["presta_R"].isin(["etab_priv", "Supplement_hospit", "Pharma_hospit",
                                                   "pharma_liste_sus"])))
# here a lot of different types of concepts (cost domain, etc.)
# we put by default inpatient, but some of those will have to be
# more precise -> this corresponds to French Health Accounts
# some of those are actually not inpatient, for instance "prise en charge entre 8h et 12h"
# is probably outpatient and some of those also have to deal with emergency
# -> but only private sector here, for the public it will be easier to distinguish
# between inpatient, outpatient and emergency (~ different tables in PMSI)
# pharma_hospit: retrocession -> do we consider as Inpatient Visit ?

ir_nat_v = define_concept(ir_nat_v["presta_R"].isin(["etab_priv", "Supplement_hospit", "Pharma_hospit",
                                                     "pharma_liste_sus"]),
                          inpatient)

# Visit to non hospital institutions -----------------
non_hospit_inst = concept.loc[(concept["vocabulary_id"]=="Visit") &
            (concept.concept_name.str.startswith("Non-hospital")),
            ["concept_id", "concept_name", "vocabulary_id"]]

test = visualize_table((ir_nat_v["presta_R"].isin(["etab_medicaux_sociaux",
                                                   "forfait_thermal",
                                                   "cures_thermales"])))
ir_nat_v = define_concept(ir_nat_v["presta_R"].isin(["etab_medicaux_sociaux",
                                                   "forfait_thermal",
                                                   "cures_thermales_heberg",
                                                   "cures_thermales"]),
                          non_hospit_inst)
# -> actually there are a bit of hospice visit (etab_medicaux_sociaux)
# in there but it had very poor coverage
# in the SNDS



# For IJ, prestations en especes et prevention IJ maladie AT, non indiv ou pro
# we put "NO_VISIT_CONCEPT" as there are no visit associated
no_visit = pd.DataFrame({"concept_id": ["NO_VISIT_CONCEPT"],
                                "concept_name": ["NO_VISIT_CONCEPT"],
                                "vocabulary_id": ["NO_VISIT_CONCEPT"]})

test = visualize_table((ir_nat_v["presta_R"].isin(["IJ",
                                                   "a_exclure",
                                                   "non_indiv_ou_pro"])))
ir_nat_v = define_concept(ir_nat_v["presta_R"].isin(["IJ",
                                                   "a_exclure",
                                                   "non_indiv_ou_pro"]),
                          no_visit)

# specific case in the expenditure DREES database that can be
# easily mapped
# - orthodontie -> Office Visit
ir_nat_v = define_concept(ir_nat_v["presta_R"].isin(["orthodontie"]),
                          ofvi)


test = visualize_table((ir_nat_v["presta_R"].isin(["autre_forfait"])))
# between inpatient visit, NO_VISIT_CONCEPT, Hospice visit (but those codes are never used actually - GIR,etc)

# audio : purchase of elements for earing problems -> Office visit ?
# optical: same issue ?
# not a service, not really a visit therefore, but a purchase
# (dans la section BIENS de la CONSOMMATION DE SERVICES ET DE BIENS MEDICAUX)


# Prevention and cash allowances -> need to separate them in to
# cash allowances NO_VISIT_CONCEPT and prevention -> Office visit
test = visualize_table((ir_nat_v["presta_R"].isin(["prestations_en_espece_et_prevention_IJ_maladie_AT"])))
# test.to_excel("resources/temp.xlsx")
# Hand labeling for prevention stored in temp.xlsx
test = pd.read_excel("resources/temp.xlsx")
test = test.loc[test["top_prev"]=="prevention",:]
ir_nat_v["top_prev"] = np.where(ir_nat_v["local_concept_code"].isin(test["local_concept_code"]),
                                1,0)
ir_nat_v = define_concept(ir_nat_v["top_prev"]==1,
                          ofvi)
ir_nat_v = define_concept((ir_nat_v["top_prev"]==0) &
                          (ir_nat_v["presta_R"].isin(["prestations_en_espece_et_prevention_IJ_maladie_AT"])),
                          no_visit)

# cures thermales hebergement and cures thermales transp : NO_VISIT_CONCEPT

ir_nat_v = define_concept(ir_nat_v["presta_R"].isin(["cures_thermales_transp", "cures_thermales_heberg"]),
                          no_visit)

# forfait_techniq
test = visualize_table(ir_nat_v["presta_R"].isin(["forfait_techniq"]))
# could be NO_CONCEPT_ID but correspond to someone who is an Inpatient
# These should actually go in PROCEDURE_OCCURRENCE table
# for now we put NO_CONCEPT_ID since the visit should be recorded in another line
# in the DCIR
ir_nat_v = define_concept(ir_nat_v["presta_R"].isin(["forfait_techniq"]),
                          inpatient)

# transport -----------------------------------------
# I have some doubts about this because in the OMOP vocabulary there
# is ambulance visit but in the Laura example https://www.ohdsi.org/wp-content/uploads/2018/10/20181010-OHDSI-Vocabulary-CDM-Tutorial.pdf
# the ambulance is parked in the Observation table
# Could not get more info on Ambulance visit
# still we want to record this so we'll put ambulance visit
# and suppress it if not needed
ambul = concept.loc[(concept["vocabulary_id"]=="Visit") &
            (concept.concept_name.str.startswith("Ambulance")),
            ["concept_id", "concept_name", "vocabulary_id"]]

test = visualize_table(ir_nat_v["presta_R"].isin(["transport"]))
# only select transport linked with ambulance (for now not taxi
# although they are counted in French Health Accounts transport)
ir_nat_v = define_concept((ir_nat_v["presta_R"].isin(["transport"]) )&
                          (ir_nat_v["PRS_NAT_CB2"].isin(["FUI", "FTU", "CTU","FUS","FUE",
                                                        "ABA", "VSL", "SMU", "ABG"])),
                          ambul)
ir_nat_v = define_concept((ir_nat_v["presta_R"].isin(["transport"]) )&
                          (ir_nat_v["PRS_NAT_CB2"].isin(["FUI", "FTU", "CTU","FUS","FUE",
                                                        "ABA", "VSL", "SMU", "ABG"])==False),
                          no_visit)

# For LPP -> these should actually go in the DEVICE_EXPOSURE table
# with the table affinée ER_TIP_F
# could be linked to a visit but do not hold code for this visit
# I created the device_exposure.json in individual mapping to store this info
# so the device_exposure will be mapped to visit occurrence via visit_occurrence_id
# therefore we will put Office Visit
test = visualize_table(ir_nat_v["presta_R"].isin(["optique_autre", "audio_autre", "LPP_autre"]))
# for optical and audio we put Office Visit and for
# LPP_autre it depends on the care site so we put the same conditional
# as for techniq and cliniq
ir_nat_v = define_concept(ir_nat_v["presta_R"].isin(["audio_autre", "optique_autre"]),
                          ofvi)
ir_nat_v = define_concept(ir_nat_v["presta_R"].isin(["LPP_autre"]),
                          cliniq_techn)


# Autre
test = visualize_table(ir_nat_v["presta_R"].isin(["Autre"]))
ir_nat_v = define_concept(ir_nat_v["presta_R"].isin(["Autre"]),
                          no_visit)

# forfait_dialyse
# we put outpatient -> because usually done in hospital structures
# without hospitalization, but some of those correspond to training for dialysis at home
# which are difficult to classify
outpatient = concept.loc[(concept["vocabulary_id"]=="Visit") &
            (concept.concept_name.str.contains("Outpatient")),
            ["concept_id", "concept_name", "vocabulary_id"]]
test = visualize_table(ir_nat_v["presta_R"].isin(["forfait_dialyse"]))
ir_nat_v = define_concept(ir_nat_v["presta_R"].isin(["forfait_dialyse"]),
                          outpatient)

# autre_forfait
# actually hospice seems to be out of the vocab
hospice = concept.loc[(concept["vocabulary_id"]=="Visit") &
            (concept.concept_name.str.contains("[hH]ospice")),
            ["concept_id", "concept_name", "vocabulary_id"]]
# this is empty

test = visualize_table(ir_nat_v["presta_R"].isin(["autre_forfait"]))
test.to_excel("resources/temp2.xlsx")
# hand labeling"
test = pd.read_excel("resources/temp2.xlsx")
test["non_hospital"] = np.where(test["non_hospital"].isnull(), "0", test["non_hospital"])
test["non_hospital"].value_counts()
ir_nat_v = define_concept((ir_nat_v["presta_R"].isin(["autre_forfait"])) & \
                          (ir_nat_v["local_concept_code"].isin(test.loc[test["non_hospital"]==0,
                                                                       "local_concept_code"])),
                          inpatient)
ir_nat_v = define_concept(ir_nat_v["presta_R"].isin(["autre_forfait"]) &
                          ir_nat_v["local_concept_code"].isin(test.loc[test["non_hospital"]==1,
                                                                       "local_concept_code"]),
                          non_hospit_inst)
ir_nat_v = define_concept(ir_nat_v["presta_R"].isin(["autre_forfait"]) &
                          ir_nat_v["local_concept_code"].isin(test.loc[test["non_hospital"]=="no_concept",
                                                                       "local_concept_code"]),
                          no_visit)

# Mapping statistics ---------------------------------
size_nomenclature = ir_nat_v.shape[0]
mapped_nomenclature = ir_nat_v.loc[ir_nat_v["proposed_concept_id"].isnull() == False, "proposed_concept_id"].count()
print(mapped_nomenclature/size_nomenclature)  # 92 %
# Drawback of this measure: Bear in mind that the IR_NAT_V nomenclature has a lot of code
# but expenditure and acts are concentrated on only a few of them

# Output to csv ---------------------------------------
ir_nat_v = ir_nat_v.loc[:,["local_concept_code", "local_concept_name", "proposed_concept_id", "name_concept_id",
                           "commentary"]]
ir_nat_v.to_csv("resources/ir_nat_v_mapping.csv", index=False)
# End of code -----------------------------------------

test = ir_nat_v.loc[ir_nat_v.proposed_concept_id.isnull(),:]