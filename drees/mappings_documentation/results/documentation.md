---
title: "Documentation on the mapping of the SNDS to the OMOP model"
date: 09-05-2019
authors: Matthieu Doutreligne
---

# Terminologies
Some terminologies have been extracted from the bigoudi with a frequence of use (4 distinct categories of frequences: $[1, 2, 3, 4] = [0.0001, 0.01, 0.05, 0.10]$)
They have been pushed to the [interchu framagit](https://framagit.org/interchu/conceptual-mapping/tree/master/terminologies/snds).

**Transmitted terminologies:**
+ gender : BEN_SEX_COD
+ dcir_statut_etablissement : ER_ETE_F__ET_CAT_COD
+ ccam : ER_CAM_F__CAM_PRS_IDE (IR_CCAM)
+ pmsi_mouvement_code : MCO_B__SOR_MOD, MCO_B__ENT_MOD (MS_SOR_V)
+ specialite_ou_nature_activite : PSE_SPE_COD, PSE_ACT_NAT (IR_PFS_V)
+ nature_prestation : PRS_NAT_REF (IR_NAT_V)
+ cip13 : ER_PHA_F__PHA_PRS_C13 (IR_PHA_V)
+ numero_ald_sur_liste : IR_IMB_R__IMB_ALD_NUM (IR_IMB_V)
+ cim10 : ... ()

**TODO**:
+ pmsi_statut_etablissement (SOC_RAI)

# Mapping

## care_site

+ PMSI_E__STA_ETA:  PX_ETA_V, vérifier après aplatissement si ces deux là sont associés, vérifier les valeurs de STA_ETA,

+ place_of_service_source_value: pas de colonne identifiée, **TODO :** Parler à Antoine Lammer de ceci 

## visit_occurrence

Visit_concept_ID: We also have to describe a scanner/echographie visit which is very common in France and is not really related to outpatient visit, neither labvisit. What to do with RIP-psy and SSR ? (Albert)

+ Standard visit visit_type_concept_id are the source from which the visit occurrence comes: *pharmacy claim, medical claim, mmedical professional claim, EHR billing record, EHR encounter record...** The more general ID for the snds could be 32035 (EHR encounter record) or even 44818518 (EHR record)
+ The visit concept ID is from the visit vocabulary among *emergency room, inpatient visit, laboratory visit, intensive care, pharmacy visit, office visit, home visit...*. It could  be obtained from a mapping with PRS_NAT_REF code.
+ admitted_from_concept_id is a detail on the location from which the patient is admitted and comes from the CMS place of service vocabulary. These are such as *office, inpatient long term care, telemedicne, public health clinic, place of employment-worksite...* It is marked as necessary but I am not sure what could be the relevant information in the SNDS.

### from ER_PRS_F

The rows should be extracted from the er_prs_f table and not from the flat table (DCIR) in order to have one line per visit_occurrence (if we make a distinct it is not a problem)

+ visit_type_concept_id: should be mapped to a general value from the visit type vocabulary such as *32034*, visit derived from EHR billing record or *44818517*, Visit derived from EHR record.
+ visit_source_concept_id: conceptual mapping is doing this to map to IR_NAT_V=snds_natureprestation.
+ visit_source_value: PRS_NAT_REF 659 mapped to IR_NAT_V and 752 in raw data, 11K are without object and 12K unknown.
+ care_site: One-to-one join with ER_ETE_F ?
+ admitted_from_source_value and discharge_to_source_value: I don't have a lot of ideas.....there is EXE_LIE_COD (lieu d'execution),

### from MCO_B/SSR_B

+ "visit_type_concept_id": "32035", encounter record sent from the hospital to the national health insurance (CNAM), not billing. From [formation interne au pmsi](https://drive.google.com/drive/folders/1Kq7Ot_BEVt2O1dk5boq8gevJFJYF4Fij)
+ admitted_from_source_value: *MCO_B__ENT_MOD* {7: transfert unité de soins, npn déterminées, 8: domicile, 6: mutation unité de soin non déterminée}. It should be more accurate than using *MCO_B__ENT_PRV* which has a lot of null and specifies the type of hospitalisation unit where the patient is discharged (eg: MCO, SSR...)
+ discharge_to_source_value: *MCO_B__SOR_MOD* same vocabulary as *MCO_B__ENT_MOD* (+ code 9: décès)/ MCO_B__SOR_DES (same vocabulary) as  MCO_B__ENT_PRV
+ Have to create and send on omop.mapper the dictionnary for IRdrees_MVT_MOD
+ visit_concept_ID could be 9201, Inpatient visit always by default. Albert tells that it is possible to catch emergency room, intensive care visits at least in the RUM. 
We also have to describe a scanner/echographie visit which is very common in France and is not really related to outpatient visit, neither labvisit.

### from MCO_FASTC/ SSR_FASTC
+ "visit_type_concept_id": "32035", should be the same as MCO_B: encounter record
+ "visit_concept_id": "9202", Outpatient Visit seems the more appropriate for extern consultations
+ Do not have any idea for admitted_from_source_value and discharge_to_concept_id

## condition_occurrence

### from MCO_D
+ "condition_type_concept_id": "44786629, secondary condition"
+ "condition_status_source_value", not clear what is meant here, I put "diagnostic associé"
+ "condition_status_concept_id", 4309641 secondary diagnosis in thhe snomed vocabulary-qualifier value class

### from MCO_B
+ "condition_status_concept_id": "4230359",

## procedure_occurrence

### from MCO_A

+ "modifier_source_value": "MOD_ACT", find the existence of this nomenclature. Should be this [nomenclature](http://docs.collectif-cocoa.org/index.php?title=Modificateurs) (to confirm with Antoine Lamer (TODO)).
+ "modifier_concept_id": "snds_modifier_concept(MOD_ACT)" find and create
+ "MCO_A__PHA_ACT", check modalities and compare to XC_NOT_R: Not coherent, variable is not interessant. Only 0, 1, 2, 3 and x with 656/710million of 0.
+ "procedure_type_concept_id": nothing making sense in the procedure type vocabulary (maybe check with pha_act...)

## observation

This table should contain every interesting information that could not be put in another table


# Mapping conceptuel

## pmsi_mouvement_code

Je n'arrive pas à mapper transfert normal/mutation/transfert pour ou après réalisation d'un acte

## condition_occurence
Comment spécifier le diagnostic ethiologique des tables SSR ? (SSR_B__ETL_AFF) dans condition_type ? condition_status ??
