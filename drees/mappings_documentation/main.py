import os
import sys
from datetime import datetime
sys.path.append('../../')
from drees.mappings_documentation.src.utils import all_mappings_to_csv, union_mappings


MY_DATE = datetime.now().date()
MAPPINGS_CONSTRUCTION_ROOT = os.path.join(os.getcwd(), '.')
PATH2RESULTS = 'results'
PATH2MAPPINGS = os.path.join(MAPPINGS_CONSTRUCTION_ROOT, 'individual_mappings')
PATH2ALL_TABLES_MAPPINGS = os.path.join(MAPPINGS_CONSTRUCTION_ROOT, PATH2RESULTS, 'all_tables_mappings.json')
# PATH2COMMENTARIES = os.path.join(MAPPINGS_CONSTRUCTION_ROOT, 'commentaries.md')
PATH2CSV = os.path.join(MAPPINGS_CONSTRUCTION_ROOT, PATH2RESULTS, 'documentation_etl_drees.xlsx')

if __name__ == '__main__':
    union_mappings(PATH2MAPPINGS, PATH2ALL_TABLES_MAPPINGS)
    all_mappings_to_csv(PATH2MAPPINGS, PATH2CSV)
