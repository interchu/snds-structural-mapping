# ETL Implementation of the SNDS at the Drees


This folder contains the schemas and codes necessary to implement the CDM Omop on the SNDS in the computing environment of the Drees.

More precisely it is built upon:
 + a flattened version of the SNDS following [the CNAM/Polytechnique methodology](https://github.com/X-DataInitiative/SCALPEL-Flattening) 
 + spark to implement all transformations
 + [mappings_documentation](mappings_documentation): structural mappings documented in json
 + *Deprecated ( [ETL implementation](etl_implementation): Notebooks that implements on our server the omop construction.)*
   

# Content
+ [ressources](resources): 
    + [bigoudi_echantillon_nomenclatures](resources/bigoudi_echantillon_nomenclatures): Les tables de nomenclatures locales (drees) nécessaires 
    au mapping conceptuel
    + [omop_schemas_dl](resources/omop_schemas_dl): the schemas of the OMOP cdm for postgres 
    [downloaded here](https://github.com/OHDSI/CommonDataModel/tree/master/PostgreSQL)