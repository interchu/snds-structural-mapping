# Mapping du SNDS vers OMOP

OMOP est un système 

# Contenu et organisation du projet

+ [echantillon_mapping](echantillon_mapping) : Le mapping de l'échantillon s'effectue grâce à des notebooks en suivant la méthodologie développé par l'équipe WIND de l'APHP. Il y a un notebook par table cible (table omop).

+ [notebooks](notebooks) : Des tests et des explorations

+ [snds-structural-mapping](snds_structural_mapping): une synchronisation du repo présent sur framagit dans le groupe interchu avec de la documentation sur la procédure de mapping.

+ [utils](utils): Des fonctions auxiliaires pour le mapping
 
