import hashlib
import json
from functools import reduce
from typing import Dict

import numpy as np
import pyspark.sql.functions as func
from pyspark import SparkContext, SparkConf
from pyspark.sql import DataFrame
from pyspark.sql import SQLContext
from pyspark.sql.types import StructType


def bigoudi_sqlContext(nbCores=20, locality='spark://localhost:7077'):
    if locality == 'dev':
        sc = SparkContext(
            conf=SparkConf().setAll([('spark.master', 'local'), ('spark.cores.max', '4')]))
        return SQLContext(sc)

    if locality == 'local':
        locality = 'local[{}]'.format(nbCores)
    conf = SparkConf().setAll([
        ('spark.master', locality),
        ('spark.executor.cores', '10'),
        ('spark.cores.max', '{}'.format(nbCores)),
        ('spark.executor.memory', '100g'),
        ('spark.driver.memory', '100g'),
        ('spark.sql.shuffle.partitions', '700'),
        ('spark.default.parallelism', '700'),
        ('spark.driver.extraJavaOptions',
         '-Djava.io.tmpdir=/home/commun/echange/matthieu_doutreligne/tmp/'),
        ('spark.executor.extraJavaOptions',
         '-Djava.io.tmpdir=/home/commun/echange/matthieu_doutreligne/tmp/'),
        ('spark.driver.maxResultSize', '20g'),
        ('spark.dynamicAllocation.enabled', 'true'),
        ('spark.shuffle.service.enabled', 'true'),
        ("spark.sql.execution.arrow.enabled", "true")
    ])
    sc = SparkContext(conf=conf)
    sqlContext = SQLContext(sc)
    return sqlContext


def unionAll(*dfs: DataFrame):
    return reduce(DataFrame.unionAll, dfs)


def get_mapping(path2mapping, table_cible, table_source):
    with open(path2mapping, 'r') as f:
        mapping_cible = json.load(f)['tables'][table_cible]['source_tables']
    if table_source in mapping_cible.keys():
        mapping = mapping_cible[table_source]['CDM_columns']
        print('------')
        print('Wrongly evaluated columns:')
        for k, col in mapping.items():
            try:
                mapping[k] = eval(mapping[k])
            except:
                print(k, mapping[k])
        print('------')
        return mapping
    else:
        print('table_cible not available, enter one of: {}'.format(mapping_cible.keys()))


def build_source_table(
        source_table: DataFrame,
        source_table_name: str,
        mapping: Dict,
        cdm_schema: StructType,
        verbose: bool = True,
        debug: bool = False) -> DataFrame:
    """
    Create a source table given a json mapping from source to omop table
    :param source_table:
    :param source_table_name:
    :param mapping:
    :param cdm_schema:
    :param verbose:
    :return:
    """
    for k, v in mapping.items():
        if debug:
            print(k, v)
        source_table = source_table.withColumn(k, v)
    source_table = source_table.select(cdm_schema.names)
    source_table = source_table.withColumn('source_table_name', func.lit(source_table_name))
    if verbose:
        source_table.printSchema()
    return source_table


def sha1_id_creator(s: str) -> np.int64:
    """string to int encoder with sha1 hash,
        Having an input space of size k and an output size of size N (here N=2^64),
        the probability to have two distincts input ids mapped to one unique hashed key is given by:
        N!/(N^k * (N-k)!) approximated by exp(-k(k-1)/(2N)))
        For our usage, this sums up to k = 3e9 and N = 1.8e19 so p(collision) = 0.25
    """
    return int(hashlib.sha1(s.encode('utf-8')).hexdigest()[:16], 16)
