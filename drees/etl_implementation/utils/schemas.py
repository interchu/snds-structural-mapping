import pandas as pd 
import re
from pyspark.sql.types import IntegerType, StringType, TimestampType, LongType, DateType, DoubleType

mapping_format = {
    "INTEGER": IntegerType(),
    "BIGINT": LongType(),
    'NUMERIC': DoubleType(),
    'DATE': DateType(),
    "TIMESTAMP": TimestampType(),
    'VARCHAR(1)': StringType(),
    'VARCHAR(3)': StringType(),
    'VARCHAR(2)': StringType(),
    'VARCHAR(9)': StringType(),
    'VARCHAR(10)': StringType(),
    'VARCHAR(20)': StringType(),
    'VARCHAR(25)': StringType(),
    "VARCHAR(50)": StringType(),
    'VARCHAR(60)': StringType(),
    'VARCHAR(100)': StringType(),
    'VARCHAR(250)': StringType(),
    'VARCHAR(255)': StringType(),
    'VARCHAR(1000)': StringType(),
    'VARCHAR(2000)': StringType(),
    'TEXT': StringType()
}

def parse_postgres(psql_raw_txt):
    tables_dic = {}
    opened_table = False
    for l in psql_raw_txt:
        if l.startswith("CREATE TABLE"):
            tab_name = re.search("CREATE TABLE (\w\w*)", l).group(1)
            opened_table = True
            current_table_lines = []
        elif opened_table == True:
            if l == ')\n':
                opened_table = False
                tables_dic[tab_name] = current_table_lines
            else:
                if l != '(\n':
                    ll = re.sub(",$", "", l.strip()).strip()
                    ll = re.sub("NOT NULL", "not_null", ll)
                    ll = re.sub("\s+", "\t", ll)
                    ll = re.sub("\t\t*", "\t", ll)
                    item = tuple(ll.split("\t"))
                    current_table_lines.append(item)
    return tables_dic

def build_spark_schemas(path2schema):
    with open(path2schema, "r") as f:
        psql_raw_txt = f.readlines()
    tables_dico = parse_postgres(psql_raw_txt)
    cleaned_tables = {}
    for k, table in tables_dico.items():
        #print(k)
        cleaned_tables[k] = pd.DataFrame.from_records(table, columns = ["field", "format", "nullable"])
        cleaned_tables[k].nullable = cleaned_tables[k].nullable.map({"not_null": False, "NULL": True}) 
        cleaned_tables[k].format = cleaned_tables[k].format.map(mapping_format) 
        # check if all types are mapped to a spark type
        if cleaned_tables[k].format.isnull().sum() >= 1:
            print(k)
    return cleaned_tables