# Mapping structurel du SNDS

Partage d'informations sur le mapping structurel du [SNDS](https://www.snds.gouv.fr/SNDS/Accueil) vers [OMOP](https://www.ohdsi.org/data-standardization/the-common-data-model/)

Les différents organismes participant au projet sont invités à déposer à un format facilement exploitable humainement leurs propositions de mapping dans un folder au nom de leur organisme.

La chaîne de discussion pour ce projet est sur le [zulip du groupe interChu](https://interchu.zulipchat.com/#narrow/stream/193675-SNDS).

# Organismes

+ [Drees](drees/)

